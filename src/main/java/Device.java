import java.util.Objects;
//Создать класс Device, который содержит 2 поля:

public class Device {

    public Processor processor;
    public Memory memory;

    public Device(){

    }
    public Device(Processor processor, Memory memory){
        this.processor = processor;
        this.memory = memory;
    }

    public void save(String[] data){
        for (String datum : data) {
            boolean added = memory.save(datum);
            if (!added) {
                System.out.println("Error - not enough memory");
                break;
            }
        }
    }

    public String[] readAll() {
        String[] stringa = new String[0];
        String stringsValue;

        while(true){
            stringsValue = memory.removeLast();
            if(stringsValue != null){
                String string = memory.removeLast();
                stringa = addStringToArray(stringa, string);
            } else {
                break;
            }
        }
        return stringa;
    }

    public void dataProcessing(){
        String[] strings = readAll();
        for (int i = 0; i < strings.length; i++){
            strings[i] = processor.dataProcess(strings[i]);
        }
        for (String string : strings) {
            memory.save(string);
        }
    }

    public String getSystemInfo(){
        return processor.getDetails() + memory.getMemoryInfo().toString();
    }

    private String[] addStringToArray(String[] strings, String string){
        String[] newStrings = new String[strings.length + 1];
        System.arraycopy(strings, 0, newStrings, 0, strings.length);
        newStrings[strings.length] = string;
        return  newStrings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return Objects.equals(processor, device.processor) && Objects.equals(memory, device.memory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processor, memory);
    }
}
