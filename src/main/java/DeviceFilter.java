public class DeviceFilter {

    public Device[] getDevicesX86(Device[] devices) {
        Device[] newDevices = new Device[0];
        for (Device device : devices) {
            Processor processor = device.processor;
            if (processor instanceof ProcessorX86) {
                newDevices = addDeviceToArray(newDevices, device);
            }
        }
        return newDevices;
    }

    public Device[] getDevicesWithLargeCache(Device[] devices){
        Device[] newDevices = new Device[0];
        for (Device device : devices) {
            if (device.processor.cache >= 100) {
                newDevices = addDeviceToArray(newDevices, device);
            }
        }
        return newDevices;
    }

    public Device[] getDevicesWithEnoughMemory(Device[] devices, int capacity){
        Device[] newDevices = new Device[0];
        for (Device device : devices) {
            int deviceCapacity = device.memory.memoryCell.length;
            if (deviceCapacity >= capacity) {
                newDevices = addDeviceToArray(newDevices, device);
            }
        }
        return newDevices;
    }

    public Device[] getDevicesWithOveroccupiedMemory(Device[] devices, int occupationPercent){
        Device[] newDevices = new Device[0];
        for (Device device : devices) {
            Memory deviceMemory = device.memory;
            MemoryInfo deviceMemoryInfo = deviceMemory.getMemoryInfo();
            int deviceOccupiedMemory = deviceMemoryInfo.getOccupiedMemory();
            if (deviceOccupiedMemory >= occupationPercent) {
                newDevices = addDeviceToArray(newDevices, device);
            }
        }
        return newDevices;
    }

    private Device[] addDeviceToArray(Device[] devices, Device device){
        Device[] newDevices = new Device[devices.length + 1];
        System.arraycopy(devices, 0, newDevices, 0, devices.length);
        newDevices[devices.length] = device;
        return  newDevices;
    }
}


