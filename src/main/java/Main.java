import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Device[] devices = new Device[10];
        for (int i = 0; i < 10; i = i + 2) {
            Processor processor = new ProcessorArm();
            Processor processor2 = new ProcessorX86();
            Memory memory = new Memory(10);
            Memory memory2 = new Memory(20);
            devices[i] = new Device(processor, memory);
            devices[i + 1] = new Device(processor2, memory2);

        }

        DeviceFilter deviceFilter = new DeviceFilter();
        System.out.println(deviceFilter.getDevicesX86(devices).length);
        System.out.println(deviceFilter.getDevicesWithEnoughMemory(devices, 10).length);
        System.out.println(deviceFilter.getDevicesWithLargeCache(devices).length);
        System.out.println(deviceFilter.getDevicesWithOveroccupiedMemory(devices, 10).length);
    }
}
