import java.util.Objects;

public abstract class Processor {
    public int frequency;
    public int cache;
    public int bitCapacity;

    public String getDetails() {

        return "Processor: " +
                "frequency = " + frequency +
                ", cache = " + cache +
                ", bitCapacity = " + bitCapacity + ". ";
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processor processor = (Processor) o;
        return frequency == processor.frequency && cache == processor.cache && bitCapacity == processor.bitCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, cache, bitCapacity);
    }
}
