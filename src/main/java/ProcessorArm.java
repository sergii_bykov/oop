import java.util.Locale;
import java.util.Objects;

public class ProcessorArm extends Processor{

    public final String architecture = "ARM";

    @Override
    public String dataProcess(String data) {
        return data.toUpperCase(Locale.ROOT);
    }

    @Override
    public String dataProcess(long data) {
        return String.valueOf(data * 2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), architecture);
    }
}
