import java.util.Arrays;

public class Memory {
    public String[] memoryCell;

    public Memory() {

    }

    public Memory(int capacity) {
        this.memoryCell = new String[capacity];
    }

    public String readLast() {
        if (memoryCell.length == 0) {
            return "Memory capacity is 0";
        }
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                return memoryCell[i];
            }
        }
        return "Memory is empty";
    }

    public String removeLast() {
        if (memoryCell.length == 0) {
            return "Memory capacity is 0";
        }
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != null) {
                String result = memoryCell[i];
                memoryCell[i] = null;
                return result;
            }
        }
        return "Memory is empty";
    }

    public boolean save(String string) {
        for (int i = 0; i < memoryCell.length; i++) {
            if (memoryCell[i] == null) {
                memoryCell[i] = string;
                return true;
            }
        }
        return false;
    }

    public MemoryInfo getMemoryInfo() {
        MemoryInfo memoryInfo = new MemoryInfo();

        int availableMemory = 0;
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] == null) {
                availableMemory++;
            }
        }

        int occupiedMemory = (int) (((double) memoryCell.length - (double) availableMemory) /
                (double) memoryCell.length * 100);
        memoryInfo.setOccupiedMemory(occupiedMemory);
        return memoryInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Memory memory = (Memory) o;
        return Arrays.equals(memoryCell, memory.memoryCell);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(memoryCell);
    }

}
